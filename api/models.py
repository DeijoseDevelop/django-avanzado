from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    pass

class Task(models.Model):
    user = models.ForeignKey(User, related_name='tasks', on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    complete = models.BooleanField(default=False)
    date = models.DateField()