from django.contrib import admin
from .models import User, Task

@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff')
    list_filter = ('is_staff', 'is_superuser', 'is_active')
    list_per_page = 25

@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'complete', 'date')
    list_filter = ('complete', 'date')
    list_per_page = 25