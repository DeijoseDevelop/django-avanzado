from django.urls import path
from .views import LoginView, DetailUser, LogoutView, RegisterView, create_task, update_task, delete_task

app_name = 'consumer'

urlpatterns = [
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('detail_user/<int:pk>/', DetailUser.as_view(), name='detail_user'),
    path('create_task/', create_task, name='create_task'),
    path('update_task/<int:pk>', update_task, name='update_task'),
    path('delete_task/<int:pk>', delete_task, name='delete_task'),
]