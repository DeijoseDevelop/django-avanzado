from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from api.models import User, Task


class LoginForm(AuthenticationForm):
    username = forms.CharField(label='Username',
                               max_length=30,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control',
                                          'placeholder': 'Enter Your username'}))
    password = forms.CharField(label='Password',
                               max_length=30,
                               widget=forms.PasswordInput(
                                   attrs={'class': 'form-control',
                                          'placeholder': 'Enter Your password'}))

class RegisterForm(UserCreationForm):
    username = forms.CharField(label='Username',
                               max_length=30,
                               widget=forms.TextInput(
                                   attrs={'class': 'form-control',
                                          'placeholder': 'Enter Your username'}))
    email = forms.EmailField(label='Email',
                             widget=forms.EmailInput(
                                 attrs={'class': 'form-control',
                                        'placeholder': 'Enter your email'}))
    first_name = forms.CharField(label='First Name',
                                 max_length=30,
                                 widget=forms.TextInput(
                                     attrs={'class': 'form-control',
                                            'placeholder': 'Enter your first name'}))
    last_name = forms.CharField(label='Last Name',
                                max_length=30,
                                widget=forms.TextInput(
                                    attrs={'class': 'form-control',
                                           'placeholder': 'Enter your last name'}))

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')


class CreateTask(forms.ModelForm):
    title = forms.CharField(label='Title',
                            max_length=30,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control',
                                       'placeholder': 'Enter your title'}))
    complete = forms.BooleanField(label='Complete',
                                    required=False,
                                    widget=forms.CheckboxInput(
                                        attrs={'class': 'form-check-input',
                                               'type': 'checkbox',
                                               'role': 'switch',
                                               'id': 'flexSwitchCheckDefault'}))
    date = forms.DateField(label='End Date',
                               widget=forms.DateInput(
                                   attrs={'class': 'form-control input-element',
                                          'placeholder': 'YYYY-MM-DD'}))

    class Meta:
        model = Task
        fields = ('title', 'complete', 'date')

