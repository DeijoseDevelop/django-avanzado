from celery import shared_task
from django.core.mail import send_mail
from todo_list import settings


@shared_task
def send_email(user):
    send_mail(
        'AVISO',
        'Correct Login',
        settings.EMAIL_HOST_USER,
        [user.email]
    )