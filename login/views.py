from django.shortcuts import render, redirect
from api.models import User, Task
from django.views.generic import FormView, CreateView, DetailView, RedirectView
from .forms import LoginForm, RegisterForm, CreateTask
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from .tasks import send_email


class LoginView(FormView):
    form_class = LoginForm
    template_name = 'consumer/login.html'
    success_url = reverse_lazy('consumer:detail_user')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            send_email(user)
            login(self.request, user)
            return super(LoginView, self).form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self):
        return reverse_lazy('consumer:detail_user', kwargs={'pk': self.request.user.pk})


class DetailUser(LoginRequiredMixin, DetailView):
    model = User
    template_name = 'consumer/detail_user.html'

    def get_context_data(self, **kwargs):
        context = super(DetailUser, self).get_context_data(**kwargs)
        user = User.objects.get(pk=self.request.user.pk)
        tasks = Task.objects.filter(user=user)
        context['user'] = user
        context['tasks'] = tasks
        return context


class LogoutView(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('consumer:login')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class RegisterView(CreateView):
    form_class = RegisterForm
    model = User
    template_name = 'consumer/register.html'
    success_url = reverse_lazy('users:login')

    def form_valid(self, form):
        form.save()
        return super(RegisterView, self).form_valid(form)


@login_required
def create_task(request):
    if request.method == 'POST':
        form = CreateTask(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.user = request.user
            task.save()
            return redirect(reverse_lazy('consumer:detail_user', kwargs={'pk': request.user.pk}))
    else:
        form = CreateTask()
    return render(request, 'consumer/create_task.html', {'form': form})


@login_required
def update_task(request, pk):
    task = Task.objects.get(pk=pk)
    if request.method == 'POST':
        form = CreateTask(request.POST, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            task.user = request.user
            task.save()
            return redirect(reverse_lazy('consumer:detail_user', kwargs={'pk': request.user.pk}))
    else:
        form = CreateTask(instance=task)
    return render(request, 'consumer/create_task.html', {'form': form})

@login_required
def delete_task(request, pk):
    task = Task.objects.get(pk=pk)
    task = task.delete()
    return redirect(reverse_lazy('consumer:detail_user', kwargs={'pk': request.user.pk}))
